//
//  SignUpScreen4Controller.swift
//  hedzup
//
//  Created by Hilary Chipunza on 1/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit

class SignUpScreen4Controller: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var digitOne: UITextField!
    
    @IBOutlet weak var digitThree: UITextField!
    @IBOutlet weak var digitTwo: UITextField!
    
    @IBOutlet weak var digitFour: UITextField!
    
    let limitLength = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        digitOne.delegate = self
        
        digitTwo.delegate = self
        
        digitThree.delegate = self
        
        digitFour.delegate = self
        
        digitOne.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        digitOne.layer.borderWidth = 1
        
        digitTwo.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        digitTwo.layer.borderWidth = 1
        
        digitThree.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        digitThree.layer.borderWidth = 1
        
        digitFour.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        digitFour.layer.borderWidth = 1
        
        digitOne.addTarget(self, action: #selector(SignUpScreen4Controller.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        digitTwo.addTarget(self, action: #selector(SignUpScreen4Controller.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        digitThree.addTarget(self, action: #selector(SignUpScreen4Controller.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        digitFour.addTarget(self, action: #selector(SignUpScreen4Controller.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
    }

     func textFieldDidChange(_ textField: UITextField)
     {
        
        
            switch textField
            {
            case digitOne:
                digitTwo.becomeFirstResponder()
            case digitTwo:
                digitThree.becomeFirstResponder()
            case digitThree:
                digitFour.becomeFirstResponder()
            case digitFour:
                digitFour.resignFirstResponder()
            default:
                break
            }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= limitLength
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



