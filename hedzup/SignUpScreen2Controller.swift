//
//  SignUpScreen2Controller.swift
//  hedzup
//
//  Created by Hilary Chipunza on 1/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit
import PasswordTextField

class SignUpScreen2Controller: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    

    @IBOutlet weak var uploadPic: UIButton!
    
    @IBOutlet weak var addPicLabel: UILabel!
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var firstname: UITextField!
    @IBOutlet weak var lastname: UITextField!
  
    @IBOutlet weak var passwordField: PasswordTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
        //print("this is = " + String(describing: profilePic.frame.size.height/2))
        //let cornerRad = Int (profilePic.frame.size.height/2)
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(SignUpScreen2Controller.handleSelectProfileImageView))
        profilePic.isUserInteractionEnabled = true
        profilePic.addGestureRecognizer(tapGestureRecognizer)
        
        //profilePic.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        
        firstname.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
         firstname.layer.borderWidth = 1
        
        lastname.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        lastname.layer.borderWidth = 1
        
        passwordField.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        passwordField.layer.borderWidth = 1
        
        profilePic.layer.cornerRadius = 60
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderColor = UIColor(red:  255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 100.0/100.0).cgColor
        
        profilePic.layer.borderWidth = 3
        passwordField.secureTextButton.buttonTouch()
        passwordField.showButtonWhile = PasswordTextField.ShowButtonWhile(rawValue: "always")!
        let validationRule = RegexRule(regex:"^(?!=.*?[A-Z]).{6,}$", errorMessage: "Password Must Contain Atleast 6 Characters")
        
        passwordField.validationRule = validationRule

    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func choosePic(_ sender: AnyObject)
    {
        handleSelectProfileImageView()
    }
 
    
    
    @IBAction func next(_ sender: AnyObject)
    {
        
        if passwordField.isInvalid()
        {
            //Swhos the error if the password is invalid, as an example is using an alert view but you can show it anyway you want
            let alert = UIAlertController(title: "Alert", message: passwordField.errorMessage(), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        else
        {
          //present()
        }
        
        
    }
    
    
    
    func handleSelectProfileImageView()
    {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker
        {
            //print("pikachu")
            profilePic.image = selectedImage
        }
        
        
        if (uploadPic != nil)
        {
            uploadPic.removeFromSuperview()
            addPicLabel.removeFromSuperview()
        }
        
       
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("canceled picker")
        dismiss(animated: true, completion: nil)
    }
    
    
  
}

