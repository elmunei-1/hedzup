//
//  SignUpScreen3Controller.swift
//  hedzup
//
//  Created by Hilary Chipunza on 1/10/2016.
//  Copyright © 2016 Hilary Chipunza. All rights reserved.
//

import UIKit
import DigitsKit

class SignUpScreen3Controller: UIViewController, CountryListViewDelegate, UIGestureRecognizerDelegate{
    
    
    @IBOutlet weak var countryList: UIButton!
    
    @IBOutlet weak var phoneNumber: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        countryList.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        countryList.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        countryList.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        
       countryList.setTitle("+27", for: UIControlState.normal)
        countryList.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        countryList.layer.borderWidth = 1
        
        phoneNumber.layer.borderColor = UIColor(red:  235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 100.0/100.0).cgColor
        phoneNumber.layer.borderWidth = 1
        
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func listCountries(_ sender: AnyObject)
    {
        let cv = CountryListViewController(nibName: "CountryListViewController", delegate: self)
        self.present(cv!, animated: true, completion: { _ in })
    }
    
    
    func didSelectCountry(_ country: [AnyHashable: Any]) {
        
        if let code = country["dial_code"]
        {
            //print("Selected Country :" + String(describing: code))
            countryList.setTitle(String(describing: code), for: UIControlState.normal)

        }
        //print("Selected Country : \(country["dial_code"])")
        //
        
    }
    
    
    @IBAction func confirmNumber(_ sender: AnyObject)
    {
        if let number = Int (phoneNumber.text!)
        {
            print(countryList.title(for: UIControlState.normal)! + String (describing: number))
            
            let digits = Digits.sharedInstance()
             let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
             configuration?.phoneNumber = countryList.title(for: UIControlState.normal)! + String (describing: number)
             digits.authenticate(with: nil, configuration: configuration!)
             { session, error in
             
             }
        }
        
       
        
        /*let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "verifyNumber")
         self.present(viewController, animated: true, completion: nil)*/

       
        /*let digits = Digits.sharedInstance()
        let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
        configuration?.phoneNumber = "+34"
        digits.authenticate(with: nil, configuration: configuration!)
        { session, error in
            
        }*/
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
